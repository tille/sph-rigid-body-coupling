#include "catch.hpp"
#include <iostream>
#include "types.hpp"
#include "PhaseSpace.hpp"
#include "Plane.hpp"
#include "SPH/Particle.hpp"


TEST_CASE("collision test between particle and wall","[collision test]") {

	using particle_t = SPH::Particle;
	calc_t threshold = 3;
	coord_t n ({0,0,1});
	coord_t n_norm = n.normalized();
    Plane wall(n_norm,{{0,0,0},{0,10,0},{10,10,0},{10,0,0}}, 1);

	SECTION("outside the wall") {
		particle_t particle({2,0,2});
		bool collision; calc_t distance; coord_t nearest_contact_point;
		std::tie(collision, distance, nearest_contact_point) = wall.check_for_collision(particle, 0, threshold);
		REQUIRE(collision == (distance >= -threshold && distance <= 0));
	}

	SECTION("inside the threshold area") {
		particle_t particle({1,0,-2});
		bool collision; calc_t distance; coord_t nearest_contact_point;
		std::tie(collision, distance, nearest_contact_point) = wall.check_for_collision(particle, 0, threshold);
		REQUIRE(collision == (distance >= -threshold && distance <= 0));
	}

	SECTION("inside the wall") {
		particle_t particle({4,0,-6});
		bool collision; calc_t distance; coord_t nearest_contact_point;
		std::tie(collision, distance, nearest_contact_point) = wall.check_for_collision(particle, 0, threshold);
		REQUIRE(collision == (distance >= -threshold && distance <= 0));
	}

}