# Rigid - Fluid Coupling

<a href="https://vimeo.com/149212584"><img src="https://i.vimeocdn.com/video/548360825.jpg?mw=600" /></a>

course project for the course
__Physically-based Simulation in Computer Graphics (Autumn 2015, 252-0546-00L)__

written by Till Ehrengruber, Patrick Schwarz, Eric Sinner

## Build requirements

The following libraries have to be installed on your system

	Eigen
	Boost
	Bullet

Additionally you need [PCL](http://pointclouds.org/) installed in your path for the poission surface reconstruction that is used in the export_obj script.

In order to compile the code you need a C++11 capable compiler.

Tested compilers

	gcc-5
	clang

Note that in order to compile the code on Linux with Clang you have to install libc++.

## Build

    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=RELEASE PROJECT_ROOT

## Running the simulation

__Rigid body simulation__

    ./build/rigid_body_simulation

__SPH fluid simulation__ (one way coupling)

    ./build/sph_simulation

__fluid - rigid simulation__

    ./build/sph_two_way_coupling_simulation

## Parameters

In order to achieve a better simulation you need to increase the dimensions of the outer box in Scene.hpp. Particle counts as high as 500k particles have been tested.

## Visualization

__quick visualization__
For a quick visualization you can use the WebGl renderer located in apps/pcd_viewer. Just open the containing trackball.html file using [Nw.js](https://github.com/nwjs)

<img width="600" src="docs/webgl_screenshot.png" />

__rendering__
For export to Wavefront obj files you need to change your current working directory to the directory that contains your pcd files (e.g. where you have run the simulation) and run the script export_obj.jl with julia. Note that the script can run the export in parallel when julia is started with additional workers.

Videos of the rendering can be found at:
 - sph simulation https://vimeo.com/149212584
 - fluid rigid body coupling https://vimeo.com/149214333
 
## References

[Particle-Based Fluid Simulation for Interactive Applications, Matthias Müller et al](http://www.matthiasmueller.info/publications/sca03.pdf)

http://n-e-r-v-o-u-s.com/education/simulation/week5.php

http://n-e-r-v-o-u-s.com/education/simulation/week6.php