import subprocess
import os
import bpy
import shlex
from mathutils import Vector

#subprocess.Popen

base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
base_path += "/build/"

bpy.context.scene.frame_current = 0 
bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_LocRot')


# create 
num_rigid_bodies = len(bpy.context.selected_objects)
time_steps = bpy.context.scene.frame_end - bpy.context.scene.frame_start + 1
argv = "/lib64/ld-linux-x86-64.so.2 " + base_path + "main " + str(num_rigid_bodies) + " " + str(time_steps)
# add rigid body objects
for obj in bpy.context.selected_objects:
    obj.rotation_mode = 'QUATERNION'
    quat = Vector(obj.rotation_quaternion[:])
    quat = -quat.yzwx
    # add location
    argv += " ".join(map(str,obj.location)) + " "
    argv += " ".join(map(str,quat)) + " "
    argv += " ".join(map(str,obj.scale)) + " "
    
print(argv)
# run simulation
args = shlex.split(argv)

#res = subprocess.run([" ",base_path+"main", argv], stdout=subprocess.PIPE)
res = subprocess.Popen(args,stdout=subprocess.PIPE,universal_newlines=True)  
stdout = res.communicate(input=None, timeout=12)
#res.wait(timeout=12)
#print(stdout)
float_list = list(map(float, str(stdout[0][:-1]).split(" ")))
assert(len(float_list)/num_rigid_bodies/12 == time_steps)
#time_steps = int(len(float_list)/num_rigid_bodies/12)
print(len(float_list))
print(time_steps)

# display results
for ts_ind in range(0, time_steps):
    for rb_ind in range(0,num_rigid_bodies):
        offset = 12*(num_rigid_bodies*ts_ind+rb_ind)
        # set transform
        obj = bpy.context.selected_objects[rb_ind]
        obj.location = Vector(float_list[offset:offset+3])
        # set rotation
        mat = obj.rotation_quaternion.to_matrix()
        for i in range(0,3):
            mat[i] = float_list[offset + 3*i + 3 :offset  + 3*i +6]
        obj.rotation_quaternion = mat.to_quaternion()
        
        
    # set key frame and advances
    bpy.context.scene.frame_current += 1
    bpy.ops.anim.keyframe_insert_menu(type='BUILTIN_KSI_LocRot')

bpy.context.scene.frame_current = 0 