#include "RigidBody/Simulation.hpp"
#include "Plane.hpp"
#include <btBulletDynamicsCommon.h>
#include <LinearMath/btGeometryUtil.h>
#include <fstream>
#include <vector>

void RigidBody::Simulation::addRigidBody(btVector3 position,btQuaternion orientation,btVector3 scale) {
	// add shape and motion state
	cubeShape.push_back(std::unique_ptr<btBoxShape>(new btBoxShape(scale)));
	cubeMotionState.push_back(std::unique_ptr<btDefaultMotionState>(new btDefaultMotionState(btTransform(orientation, position))));
	cubeScales.push_back(scale);
	//add rigid body
    btScalar mass = scale[0]*scale[1]*scale[2];
    btVector3 fallInertia(0, 0, 0);
    cubeShape.back()->calculateLocalInertia(mass, fallInertia);
    btRigidBody::btRigidBodyConstructionInfo cubeRigidBodyCI(mass, cubeMotionState.back().get(), cubeShape.back().get(), fallInertia);
	cubeRigidBody.push_back(std::unique_ptr<btRigidBody>(new btRigidBody(cubeRigidBodyCI)));
	
	cubeRigidBody.back().get()->setDamping(btScalar(0.01), btScalar(0.2));

	dynamicsWorld->addRigidBody(cubeRigidBody.back().get());
	// no damping?
}

void RigidBody::Simulation::read(std::vector<btScalar> data, unsigned int n) {
	auto it = data.begin();
	assert(data.size() == n*10);

	// init
	btVector3 position;
	btQuaternion orientation;
	btVector3 scale;

	// loop over
	for(int j = 0; j < n; ++j) {
		// read position
		for(int i = 0; i < 3 ; ++i){
			position[i] = *it;
			++it;
		}
		// read orientation 
		for(int i = 0; i < 4 ; ++i){
            orientation[i] = *it;
			++it;
		}
		// read scale
		for(int i = 0; i < 3 ; ++i){
            scale[i] = *it;
			++it;
		}
		// add rigid body
		addRigidBody(position,orientation,scale);
	}
}

void RigidBody::Simulation::applyForce(int index,btVector3 position,btVector3 force) {
	btTransform trans;
	auto rb_ptr = cubeRigidBody.at(index).get();
	rb_ptr->getMotionState()->getWorldTransform(trans);
	rb_ptr->applyForce(force,position - trans.getOrigin());
}

// export rigid body face - format origin - normal
std::vector<Plane> RigidBody::Simulation::getPlanes() const {
    std::vector<Plane> res_vec;
    res_vec.reserve(cubeRigidBody.size() * 6);

    btTransform trans; // temporary to store the world transformation
    btVector4 plane_eq; // temporary to store the current normal

    // convention for the vertices
    std::array<std::array<uint8_t, 4>, 6> all_vertex_indices{
		/*std::array<uint8_t, 4>{0, 1, 3, 2}, //  1  0  0
		std::array<uint8_t, 4>{4, 5, 7, 6}, // -1  0  0
		std::array<uint8_t, 4>{0, 1, 5, 4}, //  0  1  0
		std::array<uint8_t, 4>{2, 3, 7, 6}, //  0 -1  0
		std::array<uint8_t, 4>{0, 2, 6, 4}, //  0  0  1
		std::array<uint8_t, 4>{7, 3, 1, 5}, //  0  0 -1*/
		std::array<uint8_t, 4>{0, 4, 6, 2}, //  1  0  0
		std::array<uint8_t, 4>{1, 3, 7, 5}, // -1  0  0
		std::array<uint8_t, 4>{0, 1, 5, 4}, //  0  1  0
		std::array<uint8_t, 4>{2, 6, 7, 3}, //  0 -1  0
		std::array<uint8_t, 4>{0, 2, 3, 1}, //  0  0  1
		std::array<uint8_t, 4>{4, 5, 7, 6}, //  0  0 -1
	}; 

	size_t rb_idx=0; 
    // iterate over all rigid bodies
	for(const auto& rigid_body : cubeRigidBody) {
		// get world transformation
		rigid_body->getMotionState()->getWorldTransform(trans);

		// get collision shape
		auto collision_shape = dynamic_cast<btBoxShape*>(rigid_body->getCollisionShape());
		assert(collision_shape->getNumPlanes() == 6); // cuboids only

		// get plane equations
		btAlignedObjectArray<btVector3> planeEquations; // here we store all plane equations for this rb
		for (unsigned i=0; i<collision_shape->getNumPlanes(); ++i) {
			collision_shape->getPlaneEquation(plane_eq, i); // get plane equation
			planeEquations.push_back(plane_eq); // add it to planeEquations
		}

		// get vertices
		/*btAlignedObjectArray< btVector3 > verticesOut;
		btGeometryUtil::getVerticesFromPlaneEquations(planeEquations, verticesOut);*/
		std::vector<coord_t> global_vertices;
		btVector3 vtx;
		for (size_t i=0; i<collision_shape->getNumVertices(); ++i) {
			collision_shape->getVertex(i, vtx);
			vtx=trans*vtx;
			global_vertices.emplace_back(vtx[0], vtx[1], vtx[2]);

			//std::cout << "v_glob: " << vtx[0] << " " << vtx[1] << " " << vtx[2] << std::endl;
		}

		// iterate over all faces of the rb
		for (size_t i=0; i<6; ++i) {
			auto vertex_indices = all_vertex_indices[i]; // get indices of the vertices correspondign to this plane
			auto plane_vertices = { // get vertices of this plane as btVector3
				global_vertices[vertex_indices[0]],
				global_vertices[vertex_indices[1]],
				global_vertices[vertex_indices[2]],
				global_vertices[vertex_indices[3]]
			};

			// get normal
			collision_shape->getPlaneEquation(plane_eq, i);
			auto norm = trans.getBasis() * plane_eq;

			/*for (auto v : plane_vertices) {
				std::cout << "v: " << v[0] << " " << v[1] << " " << v[2] << std::endl;
			}
			std::cout << "norm: " << norm[0] << " " << norm[1] << " " << norm[2] << std::endl;*/

			// add plane
			res_vec.push_back(Plane(coord_t(norm[0],norm[1],norm[2]),plane_vertices,rb_idx));
		}

		++rb_idx;
	}
	return res_vec;
}

//not used right now
void RigidBody::Simulation::exportRB_to_PLY(std::string filename) const {
	std::vector<Plane> planes = getPlanes();

	std::ofstream out_stream;
	out_stream.open(filename+".ply");

	out_stream << "ply\n"
		<< "format ascii 1.0\n"
		<< "element vertex " << planes.size()*4 << "\n"
		<< "property float x\n"
		<< "property float y\n"
		<< "property float z\n"
		<< "element face " << planes.size() << "\n"
		<< "property list uchar int vertex_index\n"
		<< "end_header\n";

	for(const auto& plane : planes) {
		for (const auto& vertex : plane.vertices) {
			out_stream << vertex[0] << ' ' << vertex[1] << ' ' << vertex[2] << "\n";
		}
	}
	int j = 0;
	for(const auto& plane : planes) {
		out_stream << 4 << ' ' << j+0 << ' ' << j+1 << ' ' << j+2 << ' ' << j+3 << "\n";
		j+=4;
	}

	out_stream.close();
}


const std::array<std::array<btVector3,4>,6> RigidBody::Simulation::face_co
{
    std::array<btVector3,4>{btVector3(1,-1,-1),btVector3(1,-1,1),btVector3(1,1,1),btVector3(1,1,-1)},      // +x
    std::array<btVector3,4>{btVector3(1,-1,-1),btVector3(-1,-1,-1),btVector3(-1,-1,1),btVector3(1,-1,1)},  // -y
    std::array<btVector3,4>{btVector3(1,1,-1),btVector3(-1,1,-1),btVector3(-1,-1,-1),btVector3(1,-1,-1)},   // -z
    std::array<btVector3,4>{btVector3(-1,-1,-1),btVector3(-1,-1,1),btVector3(-1,1,1),btVector3(-1,1,-1)},  // -x
    std::array<btVector3,4>{btVector3(1,1,-1),btVector3(-1,1,-1),btVector3(-1,1,1),btVector3(1,1,1)},      // +y
    std::array<btVector3,4>{btVector3(1,1,1),btVector3(-1,1,1),btVector3(-1,-1,1),btVector3(1,-1,1)}	   // +z
};
