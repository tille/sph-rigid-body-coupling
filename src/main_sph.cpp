#include <fstream>
#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <limits>
#include <btBulletDynamicsCommon.h>
#include <iomanip>

#include "types.hpp"
#include "Plane.hpp"
#include "PhaseSpace.hpp"
#include "Scene.hpp"
#include "SPH/Simulation.hpp"
#include "RigidBody/Simulation.hpp"
#include "Coupling/Parameter.hpp"
#include "pcd_export.hpp"


template <typename PHASE_SPACE_T>
static void print_progress(size_t i, double time, double dt, PHASE_SPACE_T& phase_space) {
	std::cout << "\riteration " << std::setw(10) << i << ", time " << std::setw(10) << time << ",dt " << dt << ", energy " << std::setw(10) << phase_space.energy();
	fflush(stdout);
}

int main () {
	using particle_t = SPH::Particle;
	using phase_space_t = PhaseSpace<particle_t>;
	using sph_sim_t = SPH::Simulation<phase_space_t>;
	using sph_sim_params_t = sph_sim_t::Parameter;
	using coupling_params_t = Coupling::Parameter;

	std::cout     	 << "------------------------------------------------------------"
		<< std::endl << "      ____________________   ___"
		<< std::endl << "     /  ________   ___   /__/  /"
		<< std::endl << "    /  _____/  /  /  /  ___   /"
		<< std::endl << "   /_______/  /__/  /__/  /__/"
		<< std::endl << "   Eidgenoessische Technische Hochschule Zuerich"
		<< std::endl << "   Swiss Federal Institute of Technology Zurich"
		<< std::endl << "------------------------------------------------------------"
		<< std::endl << "    rigid - fluid coupling"
		<< std::endl << "------------------------------------------------------------"
		<< std::endl << "    course project for the course"
		<< std::endl << "    Physically-based Simulation (Autumn 2015, 252-0546-00L)"
		<< std::endl << "------------------------------------------------------------" << std::endl;

	sph_sim_params_t params;
	coupling_params_t coupling_params(params);

	/*
	 * initialize scene
	 */
	Scene scene;

	/*
	 * phase space initialization
	 */
	phase_space_t phase_space(100000, params.particle_mass, scene.box_dimensions, params.smoothing_length);

	scene.init_phase_space(params, coupling_params, phase_space);

	/*
	 * initialize simulation
	 */
  	assert (scene.equilibration_phase > 50);

	/*
	 * initialize walls and dam
	 */
	std::vector<Plane> walls = scene.get_world_constraints();

	/*
	 * rigid body initialization
	 */
	unsigned sub_steps = 20;
	RigidBody::Simulation rbs(params.dt,sub_steps);

	// initialize rigid bodies
	rbs.addRigidBody(btVector3(0.8, 0.3, 0.06),btQuaternion(1,0,0,0),btVector3(0.06,0.06,0.06));
	rbs.addRigidBody(btVector3(0.8, 0.5, 0.06),btQuaternion(1,0,0,0),btVector3(0.06,0.06,0.06));
	{
		auto rb_faces = rbs.getPlanes();
		walls.insert(walls.end(), rb_faces.begin(),rb_faces.end());
	}

	// output params
	std::cout << std::setw(30) << "number of particles: " << phase_space.size() << std::endl;
	std::cout << std::setw(30) << "number of rigid bodies: " << rbs.get_number_of_rigid_bodies() << std::endl;
	std::cout << "------------------------------------------------------------" << std::endl;

	/*
	 * define fluid-rb coupling force
	 */
	auto constraint_force = [&params, &coupling_params, &scene, &walls, &phase_space, &rbs] (particle_t& particle, const sph_sim_t& sim) {
		bool collision; calc_t d; coord_t contact_point;

		coord_t acceleration(0, 0, 0);
		size_t num_collision = 0;

		for (Plane& wall : walls) {
			if (wall.id == -1 && sim.iteration() > scene.equilibration_phase)
				continue;

			if (wall.id >= 0)
				std::tie(collision,d,contact_point) = wall.check_for_collision(particle.position, params.radius, coupling_params.collision_threshold);
			else
				std::tie(collision,d,contact_point) = wall.check_for_collision(particle.position, params.radius);

			if (collision) {
				acceleration += coupling_params.wall_k * wall.get_normal() * d * d;
				acceleration += coupling_params.wall_damping * particle.velocity.dot(wall.get_normal()) * wall.get_normal();
				++num_collision;
			}

			// naive damping in the equilibration_phase (values need to be adjusted if the box_dimension changes)
			if (sim.iteration() < scene.equilibrium_damping_phase) {
				particle.velocity *= 0.99;
				particle.acceleration *= 0.99;
			}
		}

		if (num_collision > 0)
			particle.acceleration += acceleration/num_collision;
	};

	sph_sim_t sim(phase_space, params, constraint_force);

	calc_t time=0;
	for (unsigned i=0; i<10000; ++i) {
		if (scene.variable_timestep && sim.iteration() == scene.equilibrium_damping_phase) {
			// note this induces some error but we don't mind since we have additional timesteps
			//  in which the system comes to rest
			params.dt/=2;
		}

		print_progress(i, time, params.dt, phase_space);

		// SPH Step
		sim.step();
		
		// bullet step
        rbs.step(); 

        // update rigid body planes
		auto rb_faces = rbs.getPlanes();
		std::copy(rb_faces.begin(), rb_faces.end(), walls.end()-rb_faces.size());

		// export
		if (i%scene.export_div == 0) {
			std::stringstream framename;
			framename << "sph_frame" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			exportSPH_to_PCD(framename.str(), phase_space);

			framename.str(""); framename << "sph_frame_surface" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			exportSPH_to_PCD(framename.str(), phase_space, true);

			framename.str(""); framename << "rb_frame" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			exportRB_to_PCD(framename.str(), walls);

			framename.str(""); framename << "rb_frame" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			rbs.exportRB_to_PLY(framename.str());
		}

		time+=params.dt;
	}
}
