#include "SPH/SmoothingKernel.hpp"
#include <math.h>

namespace SPH {
	namespace SmoothingKernel {

calc_t WPoly6::operator()(calc_t r2) const {
	static calc_t coeff = 315.0/(64.0*M_PI*pow(h,9));
	static calc_t h2 = h*h;
  assert(0 <= r2 && r2 <= h2);
	return coeff * pow(h2-r2, 3);
}

coord_t WPoly6::gradient(coord_t diff, calc_t r2) const {
  static calc_t coeff = -945.0/(32.0*M_PI*pow(h,9));
  static calc_t h2 = h*h;
  assert(0 <= r2 && r2 <= h2);
  return coeff * pow(h2-r2, 2) * diff;
};

calc_t WPoly6::laplacian(calc_t r2) const {
	static double coeff = -945.0/(32.0*M_PI*pow(h,9));
  static double h2 = h*h;
  assert(0 <= r2 && r2 <= h2);
  return coeff * (h2-r2) * (3.0*h2 - 7.0*r2);
}

// Wspiky
coord_t WSpiky::gradient (coord_t diff, calc_t r2) const {
  static calc_t coeff = -45.0/(M_PI*pow(h,6));
  calc_t r = sqrt(r2);
  assert(0 <= r && r <= h);
  return coeff * pow(h-r, 2) * diff / r;
}

// WViscosity
calc_t WViscosity::laplacian(calc_t r2) const {
  assert(0 <= r2 && r2 <= h*h);
  static calc_t coeff = 45.0/(M_PI*pow(h,6));
  return coeff * (h - std::sqrt(r2));
};

	}
}