#include <fstream>
#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <limits>
#include <btBulletDynamicsCommon.h>
#include <iomanip>

#include "types.hpp"
#include "Plane.hpp"
#include "PhaseSpace.hpp"
#include "Scene.hpp"
#include "SPH/Simulation.hpp"
#include "RigidBody/Simulation.hpp"
#include "Coupling/Parameter.hpp"
#include "pcd_export.hpp"

template <typename PHASE_SPACE_T>
static void print_progress(size_t i, double time, double dt, PHASE_SPACE_T& phase_space) {
	std::cout << "\riteration " << std::setw(10) << i << ", time " << std::setw(10) << time << ",dt " << dt << ", energy " << std::setw(10) << phase_space.energy();
	fflush(stdout);
}

int main () {
	using particle_t = SPH::Particle;
	using phase_space_t = PhaseSpace<particle_t>;
	using sph_sim_t = SPH::Simulation<phase_space_t>;
	using sph_sim_params_t = sph_sim_t::Parameter;
	using coupling_params_t = Coupling::Parameter;

	std::cout     	 << "------------------------------------------------------------"
		<< std::endl << "      ____________________   ___"
		<< std::endl << "     /  ________   ___   /__/  /"
		<< std::endl << "    /  _____/  /  /  /  ___   /"
		<< std::endl << "   /_______/  /__/  /__/  /__/"
		<< std::endl << "   Eidgenoessische Technische Hochschule Zuerich"
		<< std::endl << "   Swiss Federal Institute of Technology Zurich"
		<< std::endl << "------------------------------------------------------------"
		<< std::endl << "    rigid - fluid coupling"
		<< std::endl << "------------------------------------------------------------"
		<< std::endl << "    course project for the course"
		<< std::endl << "    Physically-based Simulation (Autumn 2015, 252-0546-00L)"
		<< std::endl << "------------------------------------------------------------" << std::endl;

	sph_sim_params_t params;
	params.dt = 0.001;
	coupling_params_t coupling_params(params);
	coupling_params.wall_k=1000;

	/*
	 * initialize scene
	 */
	Scene scene;

	/*
	 * phase space initialization
	 */
	phase_space_t phase_space(100000, params.particle_mass, scene.box_dimensions, params.smoothing_length);

	scene.init_phase_space(params, coupling_params, phase_space);

	/*
	 * initialize simulation
	 */
  	assert (scene.equilibration_phase > 50);

	/*
	 * initialize walls and dam
	 */
	std::vector<Plane> walls = scene.get_world_constraints();

	/*
	 * rigid body initialization
	 */
	RigidBody::Simulation rbs(params.dt,20);

	std::vector<btScalar> rigid_bodies;
	std::ifstream input("../import/init.txt");
	std::string line;
	while(std::getline(input, line)) {
        std::stringstream lineStream(line);
        btScalar value;
        while(lineStream >> value)
            rigid_bodies.push_back(value);
    }
	unsigned int rigid_body_count = rigid_bodies.size()/10;

	// add rigid bodies
	rbs.read(rigid_bodies,rigid_body_count); // not used for the time being
	auto rb_faces = rbs.getPlanes();
	walls.insert(walls.end(), rb_faces.begin(),rb_faces.end());

	// output params
	std::cout << std::setw(30) << "number of particles: " << phase_space.size() << std::endl;
	std::cout << std::setw(30) << "number of rigid bodies: " << rbs.get_number_of_rigid_bodies() << std::endl;
	std::cout << "------------------------------------------------------------" << std::endl;

	/*
	 * define fluid - rb coupling force
	 */
	auto constraint_force = [&params, &coupling_params, &scene, &walls, &phase_space, &rbs] (particle_t& particle, const sph_sim_t& sim) {
		unsigned int nearest_wall_idx = 0;
		calc_t nearest_distance = std::numeric_limits<calc_t>::infinity();
		coord_t nearest_contact_point;
		bool collision; calc_t d; coord_t contact_point;

		double scale = 5e-6;

		coord_t acceleration(0, 0, 0);
		size_t num_collision = 0;

		for (unsigned int i = 0; i < walls.size(); i++) {
			Plane& wall = walls[i];
			if (wall.id == -1 && sim.iteration() > scene.equilibration_phase)
				continue;

			std::tie(collision,d,contact_point) = wall.check_for_collision(particle.position, params.radius, coupling_params.collision_threshold);
			if (collision) {
				// f*** physics
				acceleration += (coupling_params.wall_k * d) * (coupling_params.wall_k * d) * (coupling_params.wall_k * d) * wall.get_normal();
				acceleration += coupling_params.wall_damping * particle.velocity.dot(wall.get_normal()) * wall.get_normal();
				if (wall.id >= 0) {
					rbs.applyForce(
						wall.id,
						btVector3(nearest_contact_point[0], nearest_contact_point[1], nearest_contact_point[2]),
						btVector3(
							scale*params.particle_mass*particle.acceleration[0],
							scale*params.particle_mass*particle.acceleration[1]/2,
							scale*params.particle_mass*particle.acceleration[2]
						)
					);
					particle.acceleration*=0.99;
					particle.velocity *= 0.99;
				}
				++num_collision;
			}

			if (sim.iteration() < scene.equilibration_phase - 100) {
				particle.velocity *= 0.9999;
				particle.acceleration *= 0.9999;
			}
		}

		if (num_collision > 0)
			particle.acceleration += acceleration/num_collision;
	};

	/*
	 * initialize fluid simulation
	 */
	sph_sim_t sim(phase_space, params, constraint_force);

	calc_t time=0;
	for (unsigned i=0; i<10000; ++i) {
		print_progress(i, time, params.dt, phase_space);

		// sph step
		sim.step();

		// rigid body step
        rbs.step(); 

        // update rigid body planes
		auto rb_faces = rbs.getPlanes();
		std::copy(rb_faces.begin(), rb_faces.end(), walls.end()-rb_faces.size());

		// export
		if (i%scene.export_div == 0) {
			std::stringstream framename;
			framename << "sph_frame" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			exportSPH_to_PCD(framename.str(), phase_space);

			framename.str(""); framename << "sph_frame_surface" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			exportSPH_to_PCD(framename.str(), phase_space, true);

			framename.str(""); framename << "rb_frame" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			exportRB_to_PCD(framename.str(), walls);

			framename.str(""); framename << "rb_frame" << std::setfill('0') << std::setw(5) << i/scene.export_div;
			rbs.exportRB_to_PLY(framename.str());
		}

		time+=params.dt;
	}
}
