#include <iostream>
#include "types.hpp"

#include "Plane.hpp"
#include "RigidBody/Simulation.hpp"

int main(int argc, char** argv) {

    //example
    /*
	
    RigidBody::Simulation sim_rb(0.1,0.2);
    sim_rb.addRigidBody(btVector3(0,0,0),btQuaternion(1,0,0,0),btVector3(1,1,1));
    auto fc_ex = sim_rb.getPlanes();
    for (auto& element : fc_ex) {
    	std::cout << element;
    	auto normal = element.get_normal();
    	std::cout << normal[0] << normal[1] << normal[2];
    	std::cout << std::endl;
    }*/
//    return 0;

	// read in parameters
	unsigned int rigid_body_count = 1;
	unsigned int iter_count = 1;
    //std::clog << "rigid_body_count = " << rigid_body_count << " ; argc = " << argc << "\n";
   
    // read in rigid bodies
    std::vector<btScalar> rigid_bodies(rigid_body_count*10);

    // read from file
    //std::ifstream rbf_init("../import/init.txt",std::ios_base::in);


    // add rigid bodies
/*	for(int i = 3; i < 10*rigid_body_count + 3; ++i) {
		rigid_bodies.push_back(std::atof(argv[i]));
    }*/
	
	// parameters
    calc_t timestep = 1.0/60.0;//params.dt;

	unsigned sub_steps = 20;
	
	// initialize simulation
	RigidBody::Simulation rbs(timestep,sub_steps);
	
	// add rigid bodies
    rbs.addRigidBody(btVector3(0.3434,0.53434,0.533),btQuaternion(1,0,0,0),btVector3(0.5,0.5,0.5));
	// loop
	for(int i = 0; i < iter_count;++i) {
		//rbs.step();
		//rbs.exportFaces("bla.blub");

        rbs.exportRB_to_PLY("bla.ply");
    }
    return 0;

}

