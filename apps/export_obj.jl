pattern = r"^sph_frame_surface([0-9]+).pcd"
unfilter_files = filter((filename) -> match(pattern, filename)!=nothing, readdir("."))
r=0:40:9000
files=[]
for filename in unfilter_files
	frame = parse(Int,match(pattern, filename).captures[1]);
	if in(frame, r)
		push!(files, filename)
	end
end

run(`mkdir obj_files`)

threads = @parallel for file in files
 outputfile = "obj_files/" * split(file, ".")[1]
 if isfile("./"*outputfile*".obj")
 	println("skipping " * outputfile) 
 	continue;
 end
 run(`pcl_poisson_reconstruction $file $file.vtk`)
 run(`pcl_vtk2obj $file.vtk $outputfile.obj`)
 run(`rm $file.vtk`)
end

pattern = r"^rb_frame([0-9]+).ply"
unfilter_files = filter((filename) -> match(pattern, filename)!=nothing, readdir("."))
files=[]
for filename in unfilter_files
	frame = parse(Int,match(pattern, filename).captures[1]);
	if in(frame, r)
		push!(files, filename)
	end
end

threads = @parallel for file in files
 outputfile = "obj_files/" * split(file, ".")[1]
 if isfile("./"*outputfile*".obj")
 	println("skipping " * outputfile) 
 	continue;
 end
 run(`/Users/tehrengruber/Downloads/ply2obj.py $file`)
 println("done " * outputfile)
end