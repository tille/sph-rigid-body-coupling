var fs = require("fs");
var assert = require("assert");

function read_pcd(filename) {
	contents = fs.readFileSync(filename, "utf-8");
	contents = contents.trim().split("\n");
	contents.reverse();

	// parse header
	var header = {};
	var line = contents.pop();
	var attr = "";
	while (!(attr = line.trim().substr(0, line.trim().indexOf(" ")).toLowerCase()).match('-?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?')) {
		// skip some data
		if (attr == "#") {
			line = contents.pop();
			continue;
		}

		header[attr] = line.substr(attr.length+1)

		switch (attr) {
			case "fields":
			case "size":
			case "type":
			case "count":
				header[attr] = header[attr].trim().replace(/ +(?= )/g,'').split(' ')
				break;
		}

		line = contents.pop();
	}
	contents.push(line);

	assert(header["data"] == "ascii")
	assert(header["points"] == contents.length)

	var positions = new Float32Array( header["points"] * 3 );
	var surface_flags = new Float32Array( header["points"] );

	for (var n=0; n<header["points"]; ++n) {
		data_line = contents.pop().split(' ');
		num_fields = header["fields"].length;
		for (var i=0; i<3; ++i) {
			positions[n*3+i] = data_line[i];
		}
		surface_flags[n] = data_line[6];
	}

	return {
		header: header,
		data: positions,
		surface_flags: surface_flags
	};
}

function read_pcd_planes(filename) {
	contents = fs.readFileSync(filename, "utf-8");
	contents = contents.trim().split("\n");
	contents.reverse();

	// parse header
	var header = {};
	var line = contents.pop();
	var attr = "";
	while (!(attr = line.trim().substr(0, line.trim().indexOf(" ")).toLowerCase()).match('-?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?')) {
		// skip some data
		if (attr == "#") {
			line = contents.pop();
			continue;
		}

		header[attr] = line.substr(attr.length+1)

		switch (attr) {
			case "fields":
			case "size":
			case "type":
			case "count":
				header[attr] = header[attr].split(' ')
				break;
		}

		line = contents.pop();
	}
	contents.push(line);

	assert(header["data"] == "ascii")
	assert(header["points"] == contents.length)

	var positions = new Array(parseInt(header["points"]));

	for (var n=0; n<header["points"]; ++n) {
		data_line = contents.pop().trim().replace(/ +(?= )/g,'').split(' ');
		num_fields = header["fields"].length;	

		/*positions[n] = {
			position: [data_line[0], data_line[1], data_line[2]],
			normal: [data_line[3], data_line[4], data_line[5]],
			width: data_line[6],
			height: data_line[7]
		};*/
		positions[n] = {
			vertices: [ // note the permuation of the vertices here!
				[data_line[0], data_line[1], data_line[2]],
				[data_line[3], data_line[4], data_line[5]],
				[data_line[9], data_line[10], data_line[11]],
				[data_line[6], data_line[7], data_line[8]]
			],
			normal: [data_line[12], data_line[13], data_line[14]]
		};
	}

	return {
		header: header,
		data: positions
	};
}