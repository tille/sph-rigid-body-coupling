#ifndef PLANE_HPP
#define PLANE_HPP

#include "types.hpp"
#include "SPH/Particle.hpp"
#include <cassert>
#include <vector>
#include <utility>
#include <iostream>
#include <tuple>


typedef std::pair<coord_t,coord_t> border_t;

class Plane {
public:
	int id;

	/*
	 * Vertices must be in order such that two consecutive vertices are connected by an edge. 
	 * Also the first and the last vertices are connected by an edge
	 */
	Plane(coord_t normal_, std::vector<coord_t> vertices_, int id_) : normal(normal_.normalized()), vertices(vertices_), id(id_) {
		assert(vertices.size() > 0);
        d = normal.dot(vertices[0]);
		for (auto x : vertices)
			assert(normal.dot(x) + d == 0);

		for (int i = 0; i < vertices.size()-1; ++i) {
			borders.push_back(std::make_pair(vertices[i],vertices[i+1]));
		}
		borders.push_back(std::make_pair(vertices.back(),vertices.front()));
	}

	Plane(std::vector<coord_t> vertices_, int id_) :
		Plane((vertices_[0]-vertices_[1]).cross(vertices_[0]-vertices_[3]), vertices_, id_) {}

	collision_query_t check_for_collision(const SPH::Particle& particle, calc_t radius) {
		calc_t distance_to_plane = (vertices[0]-particle.position).dot(normal) + radius;

		if (distance_to_plane < 0.)
			//the projection on the plane does not need to make sense because it is not used
			return std::make_tuple(false,distance_to_plane, particle.position);

		return std::make_tuple(true,distance_to_plane, particle.position);
	}

	/*
	 * Calculates the distance from the particle to the plane and checks if it is smaller than threshold.
	 * It does so by projecting the particle on an infinite plane comprising the plane and then finding the direct path to the plane
	 */
	collision_query_t check_for_collision(const SPH::Particle& particle, calc_t radius, calc_t threshold) {

		//calc_t distance_to_plane = d + normal.dot(particle.position);
		calc_t distance_to_plane = (vertices[0]-particle.position).dot(normal) + radius;
		//if (particle.flag && distance_to_plane > 0.)
		//	std::cout << distance_to_plane << std::endl;
		//calc_t abs_distance_to_plane = std::abs(distance_to_plane);
		if (!(distance_to_plane > 0. && distance_to_plane < threshold))
			//the projection on the plane does not need to make sense because it is not used
			return std::make_tuple(false,distance_to_plane, particle.position);

		coord_t projection_on_plane = particle.position - (particle.position-vertices[0]).dot(normal) * normal;

		//check whether projection is already in the plane
		if (check_if_inside_quadrilateral(projection_on_plane)) {
			return std::make_tuple(true,distance_to_plane, projection_on_plane);
		}
		else {
			return std::make_tuple(false,distance_to_plane, projection_on_plane);
		}
	}

	void translate_plane(coord_t vector) {
		for (auto& element : vertices) {
			element += vector;
		}
		d = - normal.dot(vertices[0]);
		for (int i = 0; i < vertices.size()-1; ++i) {
			borders.push_back(std::make_pair(vertices[i],vertices[i+1]));
		}
		borders.push_back(std::make_pair(vertices.back(),vertices.front()));
	}

	coord_t get_normal() const {
		return normal;
	}

	coord_t get_vertex(unsigned int index) {
		return vertices[index];
	}

	friend std::ostream& operator << (std::ostream& stream, Plane const& plane);

private:

	/*
	 * Calculates the minimum distance from a point outside of the polygon and an edge of the polygon
	 * and returns the corresponding point on the plane
	 * (the point and the polygon are in the same infinite plane)
	 */
	coord_t calculate_minimum_distance_to_border(border_t border, coord_t point) {
		coord_t x1 = border.first;
		coord_t x2 = border.second;

		const calc_t l2 = (x1 - x2).dot(x1 - x2);
		if (l2 == 0.0) 
			return x1;

		const calc_t t = (point - x1).dot(x2 - x1) / l2;
		if (t < 0.0) 
			return x1;   
		else if (t > 1.0) 
			return x2;  

		const coord_t projection = x1 + t * (x2 - x1);
		return projection;
	}

	/*
	 * Checks if a point is inside the givn polygon (the point and the polygon are in the same infinite plane)
	 * works for now only for rectangles
	 */
	bool check_if_inside_quadrilateral(coord_t point) {
		coord_t AB = vertices[0]-vertices[1];
		calc_t AM_AB = (vertices[0]-point).dot(AB);
		calc_t AB_AB = AB.dot(AB);
		if (!(0 <= AM_AB && AM_AB <= AB_AB)) {
			return false;
		}
		coord_t AD = vertices[0]-vertices[3];
		calc_t AM_AD = (vertices[0]-point).dot(AD);
		calc_t AD_AD = AD.dot(AD);
		if (!(0 <= AM_AD && AM_AD <= AD_AD)) {
			return false;
		}
		return true;
	}

public: // todo remove
	coord_t normal;
	calc_t d;
	std::vector<coord_t> vertices;
	std::vector<border_t> borders;
};

inline std::ostream& operator<< (std::ostream& stream, const Plane& plane) {
	for (auto& element : plane.vertices) {
		stream << element[0] << "\t" << element[1] << "\t" << element[2] << "\n";
	}
	return stream;
};


#endif
