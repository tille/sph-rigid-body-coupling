#ifndef PCD_EXPORT_HPP
#define PCD_EXPORT_HPP

template <class PARTICLE_T>
void exportSPH_to_PCD(std::string filename, const PhaseSpace<PARTICLE_T>& phase_space, bool surface_only=false) {
	std::ofstream pcd_file;
    pcd_file.open(filename + ".pcd", std::ios::out);
    size_t num_particles=0;

    for (const PARTICLE_T& p : phase_space.particles()) {
    	if (!surface_only || p.flag)
			++num_particles;
	}

	pcd_file << "# .PCD v.5 - Point Cloud Data file format\n"
	         << "VERSION .5\n"
	         << "FIELDS x y z normal_x normal_y normal_z surface\n"
	         << "SIZE 4 4 4 4 4 4 4\n"
	         << "TYPE F F F F F F F\n"
	         << "COUNT 1 1 1 1 1 1 1\n"
	         << "WIDTH " << num_particles << "\n"
	         << "HEIGHT 1\n"
	         << "POINTS " << num_particles << "\n"
	         << "DATA ascii\n";

	for (const PARTICLE_T& p : phase_space.particles()) {
		if (surface_only && !p.flag)
			continue;

		for (unsigned i=0; i<p.position.size(); ++i) {
			pcd_file << p.position[i] << ' ';
		}
		for (unsigned i=0; i<p.normal.size(); ++i) {
			pcd_file << p.normal[i] << ' ';
		}
		pcd_file << (p.flag ? 1 : 0) << '\n';
	}

	pcd_file.close();
}


void exportRB_to_PCD (std::string filename, std::vector<Plane> planes) {
	std::ofstream out_stream;
	out_stream.open(filename+".pcd");

	out_stream << "# .PCD v.5 - Point Cloud Data file format\n"
	         << "VERSION .5\n"
	         << "FIELDS x1 y1 z2 x2 y2 z2 x3 y2 z3 x4 y4 z4 nx ny nz \n"
	         << "SIZE 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4\n"
	         << "TYPE F F F F F F F F F F F F F F F F\n"
	         << "COUNT 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
	         << "WIDTH " << planes.size() << "\n"
	         << "HEIGHT 1\n"
	         << "POINTS " << planes.size() << "\n"
	         << "DATA ascii\n";

	for(const auto& plane : planes) {
		for (const auto& vertex : plane.vertices) {
			out_stream << vertex[0] << ' ' << vertex[1] << ' ' << vertex[2] << ' ';
		}
		out_stream << plane.get_normal()[0] << ' ' << plane.get_normal()[1] << ' ' << plane.get_normal()[2] << "\n";
	}

	out_stream.close();
}
#endif