#ifndef RIGID_BODY_SIMULATION_HPP
#define RIGID_BODY_SIMULATION_HPP

#include <fstream>
#include <iostream>
#include <memory>
#include <vector>
#include <array>

#include <btBulletDynamicsCommon.h>

#include "Plane.hpp"

namespace RigidBody {

	class Simulation {
	public:
		/// create new rigid body simulation
		Simulation(double& time_step_,unsigned sub_steps_) : 
			time_step(time_step_),sub_steps(sub_steps_),
			
			broadphase(new btDbvtBroadphase()), 
			collisionConfiguration(new btDefaultCollisionConfiguration()),
			dispatcher(new btCollisionDispatcher(collisionConfiguration.get())),
			solver(new btSequentialImpulseConstraintSolver),
			dynamicsWorld(new btDiscreteDynamicsWorld(dispatcher.get(), broadphase.get(), solver.get(), collisionConfiguration.get())),
			
			groundShape(new btStaticPlaneShape(btVector3(0, 1, 0), 0)),
			backwallShape(new btStaticPlaneShape(btVector3(0,0,-1),-0.25)),
			frontwallShape(new btStaticPlaneShape(btVector3(0,0,1),0)),
			rightwallShape(new btStaticPlaneShape(btVector3(-1,0,0),-1)),
			leftwallShape(new btStaticPlaneShape(btVector3(1,0,0),0)),
			groundMotionState(new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0))))

		{
            dynamicsWorld->setGravity(btVector3(0, -0.980665,0));
			// add ground
			btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, groundMotionState.get(), groundShape.get(), btVector3(0, 0, 0));
			groundRigidBody.reset(new btRigidBody(groundRigidBodyCI));
			dynamicsWorld->addRigidBody(groundRigidBody.get());


       		btRigidBody::btRigidBodyConstructionInfo backwallRigidBodyCI(0, groundMotionState.get(), backwallShape.get(), btVector3(0, 0, 0));
        	backwallRigidBody.reset(new btRigidBody(backwallRigidBodyCI));
			dynamicsWorld->addRigidBody(backwallRigidBody.get());

			btRigidBody::btRigidBodyConstructionInfo frontwallRigidBodyCI(0, groundMotionState.get(), frontwallShape.get(), btVector3(0, 0, 0));
        	frontwallRigidBody.reset(new btRigidBody(frontwallRigidBodyCI));
			dynamicsWorld->addRigidBody(frontwallRigidBody.get());

			btRigidBody::btRigidBodyConstructionInfo rightwallRigidBodyCI(0, groundMotionState.get(), rightwallShape.get(), btVector3(0, 0, 0));
        	rightwallRigidBody.reset(new btRigidBody(rightwallRigidBodyCI));
			dynamicsWorld->addRigidBody(rightwallRigidBody.get());

			btRigidBody::btRigidBodyConstructionInfo leftwallRigidBodyCI(0, groundMotionState.get(), leftwallShape.get(), btVector3(0, 0, 0));
        	leftwallRigidBody.reset(new btRigidBody(leftwallRigidBodyCI));
			dynamicsWorld->addRigidBody(leftwallRigidBody.get());



		}
			
		/// add rigid body
		void addRigidBody(btVector3 position,btQuaternion orientation,btVector3 scale);

		/** read n rigid bodies from a file
		 *	format : 
		 *		float float float // position
		 *		float float float float // orientation
		 *		float float float // scale
		*/
		void read(std::vector<btScalar> data ,unsigned int n);

		/// add force 
		void applyForce(int index,btVector3 position,btVector3 force);

		/// step simulation
		void step() {
			for (size_t i=0; i<10; ++i) {
				dynamicsWorld->stepSimulation(time_step/10, sub_steps);
			}
			//dynamicsWorld->stepSimulation(time_step, sub_steps);
		}
		/// export rigid body face - format origin - normal
        std::vector<Plane> getPlanes() const;
		
		/// write states to fil/forum/e
		void exportRB_to_PLY(std::string filename) const;
		/// print a btVector3
		inline static void writeBtVector3(std::ostream& out_stream,btVector3 vec){
			// out put position
			out_stream << vec.x() << " ";
			out_stream << vec.y() << " ";
			out_stream << vec.z() << " ";
			out_stream << "\n";
		}

		size_t get_number_of_rigid_bodies() {
			return cubeRigidBody.end()-cubeRigidBody.begin();
		}
		
		/// delete stuff
		~Simulation() {
			dynamicsWorld->removeRigidBody(groundRigidBody.get());
			for(auto& rigid_body_cube : cubeRigidBody) {
				dynamicsWorld->removeRigidBody(rigid_body_cube.get());
			}
		}
	private:
		// parameters
		double& time_step; /// time step
		unsigned sub_steps; /// maximum number of sub steps
		
		// bullet stuff
		std::unique_ptr<btBroadphaseInterface> broadphase; /// broadphase
		std::unique_ptr<btDefaultCollisionConfiguration> collisionConfiguration; /// collision configuration
		std::unique_ptr<btCollisionDispatcher> dispatcher; /// collision dispatcher
		std::unique_ptr<btSequentialImpulseConstraintSolver> solver; /// constraint solver
		std::unique_ptr<btDiscreteDynamicsWorld> dynamicsWorld; /// bullet dynamic world
		
		// ground stuff
		std::unique_ptr<btCollisionShape> groundShape; /// ground collision shape
		std::unique_ptr<btRigidBody> groundRigidBody; /// ground rigid body implementation
		std::unique_ptr<btDefaultMotionState> groundMotionState; /// ground position and orientation

		// wall stuff
		std::unique_ptr<btCollisionShape> frontwallShape; /// wall collision shape
		std::unique_ptr<btRigidBody> frontwallRigidBody; /// wall rigid body implementation
		std::unique_ptr<btDefaultMotionState> frontwallMotionState; /// wall position and orientation

		std::unique_ptr<btCollisionShape> backwallShape; /// wall collision shape
		std::unique_ptr<btRigidBody> backwallRigidBody; /// wall rigid body implementation
		std::unique_ptr<btDefaultMotionState> backwallMotionState; /// wall position and orientation

		std::unique_ptr<btCollisionShape> rightwallShape; /// wall collision shape
		std::unique_ptr<btRigidBody> rightwallRigidBody; /// wall rigid body implementation
		std::unique_ptr<btDefaultMotionState> rightwallMotionState; /// wall position and orientation

		std::unique_ptr<btCollisionShape> leftwallShape; /// wall collision shape
		std::unique_ptr<btRigidBody> leftwallRigidBody; /// wall rigid body implementation
		std::unique_ptr<btDefaultMotionState> leftwallMotionState; /// wall position and orientation
		
		// cube stuff
		std::vector<std::unique_ptr<btCollisionShape>> cubeShape; //! cube rigid body states
		std::vector<std::unique_ptr<btDefaultMotionState>> cubeMotionState; //! cube motion states
		std::vector<std::unique_ptr<btRigidBody>> cubeRigidBody; //! cube rigid body states
		std::vector<btVector3> cubeScales; //! scale of the cubes

        const static std::array<std::array<btVector3,4>,6> face_co;

    };
}; // END namespace RigidBody



#endif // RIGID_BODY_SIMULATION_HPP
