#ifndef SCENE_HPP
#define SCENE_HPP

#include <cassert>
#include "Coupling/Parameter.hpp"

struct Scene {
	coord_t box_dimensions = coord_t(2*0.5, 0.5, 0.5/2);

	bool variable_timestep = true;

	size_t equilibration_phase = 500;

	size_t equilibrium_damping_phase = 300;

	Scene() { assert(equilibrium_damping_phase < equilibration_phase); }

	// export only every n steps
	size_t export_div = 1;

	template <typename SPH_SIMULATION_PARAMS_T, typename PHASE_SPACE_T>
	void init_phase_space(const SPH_SIMULATION_PARAMS_T& sph_params, const Coupling::Parameter& coupling_params, PHASE_SPACE_T& phase_space) {
		using particle_t = typename PHASE_SPACE_T::particle_t;

		calc_t dx = box_dimensions[0]; calc_t dy = box_dimensions[1]; calc_t dz = box_dimensions[2];

		auto margin = coupling_params.collision_threshold;

		for (calc_t x = margin; x < dx/4-margin; x += sph_params.smoothing_length/2) {
			for (calc_t y = margin; y < dy; y += sph_params.smoothing_length/2) {
				for (calc_t z = margin; z < dz-margin; z += sph_params.smoothing_length/2) {
					phase_space.push_back(particle_t(coord_t(x, y,z)));
				}
			}
		}
		phase_space.push_back(particle_t(coord_t(dx/6, dy/6,dz/6)));
	}

	std::vector<Plane> get_world_constraints() {
		calc_t dx = box_dimensions[0]; calc_t dy = box_dimensions[1]; calc_t dz = box_dimensions[2];

		std::vector<Plane> faces;
		faces.reserve(10);

		/* 
		 * Walls
		 */
		int outer_box_id = -2; // mark walls with this id
		faces.push_back(Plane({ 0, 1, 0}, {{0,0,0},{0,0,dz},{dx,0,dz},{dx,0,0}}, outer_box_id)); // bottom
		faces.push_back(Plane({ 0, 0,-1}, {{0,0,dz},{0,dy,dz},{dx,dy,dz},{dx,0,dz}}, outer_box_id)); // back
		faces.push_back(Plane({ 0, 0, 1}, {{0,0,0},{0,dy,0},{dx,dy,0},{dx,0,0}}, outer_box_id)); // front
		faces.push_back(Plane({ 1, 0, 0}, {{0,0,0},{0,dy,0},{0,dy,dz},{0,0,dz}}, outer_box_id)); // left
		faces.push_back(Plane({-1, 0, 0}, {{dx,0,0},{dx,dy,0},{dx,dy,dz},{dx,0,dz}}, outer_box_id)); // right

		/*
		 * Dam
		 */
		int dam_id = -1; // mark dam with this id
		/*faces.push_back(Plane({dx/3-dx/4,0,-dz/4}, {{dx/4,0,0},{dx/4,dy,0},{dx/3,dy,dz/4},{dx/3,0,dz/4}}, dam_id)); //dam back
		faces.push_back(Plane({-dz/2,0,-dx/2}, {{dx/4,0,dz},{dx/4,dy,dz},{dx/2,dy,3*dz/4},{dx/2,0,3*dz/4}}, dam_id)); //dam back
		faces.push_back(Plane({-1,0,0}, {{dx/3,0,dz/4},{dx/3,dy,dz/4},{dx/3,dy,3*dz/4},{dx/3,0,3*dz/4}}, dam_id)); //dam middle*/
		faces.push_back(Plane({-1,0,0}, {{dx/4,0,0},{dx/4,dy,0},{dx/4,dy,dz},{dx/4,0,dz}}, dam_id)); //dam middle

		return faces;
	}
};

#endif