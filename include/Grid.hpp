#ifndef GRID_HPP
#define GRID_HPP

#include <list>
#include <vector>
#include <array>

template <typename PARTICLE_T>
struct GridCell {
	typedef std::size_t idx_type;

	std::list<PARTICLE_T*> particles;
	//std::array<GridCell*, 26> neighbours;

	idx_type idx_x;
	idx_type idx_y;
	idx_type idx_z;

	bool operator==(const GridCell& cell) const {
		return idx_x == cell.idx_x && idx_y == cell.idx_y && idx_z == cell.idx_z;
	}

	bool operator!=(const GridCell& cell) const { return !(*this == cell); }
};

template <typename PARTICLE_T>
struct Grid {
	typedef std::size_t idx_type;
	typedef PARTICLE_T particle_t;
	typedef GridCell<particle_t> grid_cell_t;

	typedef boost::range_detail::filtered_range<std::function<bool(particle_t*)>, std::list<PARTICLE_T*>> neighbour_container_t;

	const coord_t dimensions;

	const calc_t smoothing_length;

	idx_type Nx;
	idx_type Ny;
	idx_type Nz;

	std::vector<grid_cell_t> cells;

	Grid(const coord_t dimensions_, const calc_t smoothing_length_)
		: dimensions(dimensions_),
		  smoothing_length(smoothing_length_),
		  Nx(ceil(dimensions[0]/smoothing_length_)),
		  Ny(ceil(dimensions[1]/smoothing_length_)),
		  Nz(ceil(dimensions[2]/smoothing_length_)),
		  cells(ceil(dimensions[0]/smoothing_length)*ceil(dimensions[1]/smoothing_length)*ceil(dimensions[2]/smoothing_length))
		  {
			for (int x = 0; x < Nx; x++) {
				for (int y = 0; y < Ny; y++) {
					for (int z = 0; z < Nz; z++) {
						get(x, y, z).idx_x = x;
						get(x, y, z).idx_y = y;
						get(x, y, z).idx_z = z;
					}
				}
			}
		  }

	template <typename PHASE_SPACE_T, typename F1, typename F2, typename F3>
	void for_each(PHASE_SPACE_T& phase_space, const calc_t rcut, const F1& pre, const F2& interaction, const F3& post) {
		// iterate over all grid cells
		for (int x = 0; x < Nx; x++) {
		    for (int y = 0; y < Ny; y++) {
		      	for (int z = 0; z < Nz; z++) {
			      	// iterate over all particles in this cell
			      	for (auto& particle_ptr : get(x, y, z).particles) {
			      		particle_t& particle(*particle_ptr); // save ptr to reference

			      		// apply pre function
			      		pre(particle);

						// iterate over all neighbouring cells
						for (int8_t offset_x = -1; offset_x <= 1; offset_x++) {
							if (x+offset_x < 0) continue;
				            if (x+offset_x >= Nx) break;

						    for (int8_t offset_y = -1; offset_y <= 1; offset_y++) {
						    	if (y+offset_y < 0) continue;
				            	if (y+offset_y >= Ny) break;

						    	for (int8_t offset_z = -1; offset_z <= 1; offset_z++) {
						    		if (z+offset_z < 0) continue;
				            		if (z+offset_z >= Nz) break;

				            		std::function<bool(particle_t*)> pred = [&rcut, &particle] 
				            		(particle_t* neighbour_ptr) {
				            			//if (isNeighbour(rcut, particle, *neighbour_ptr))
				            				//std::cout << particle.id << " " << (*neighbour_ptr).id << std::endl;

										return isNeighbour(rcut, particle, *neighbour_ptr);
									};

				            		neighbour_container_t neighbours = boost::adaptors::filter(
										get(x+offset_x, y+offset_y, z+offset_z).particles,
										pred
									);

				            		// apply interaction function
				            		for (particle_t*& neighbour_ptr : neighbours) {
				            			//std::cout << "1";
					            		// iterate over all neighbours
					            		interaction(particle, *neighbour_ptr);
					            	}
								}
							}
						}

						// apply func
						post(particle);
					}
				}
			}
		}
	}

	void update() {
		std::vector<particle_t*> moved_particles;

		// iterate over all grid cells
		for (idx_type x = 0; x < Nx; x++) {
		    for (idx_type y = 0; y < Ny; y++) {
		      	for (idx_type z = 0; z < Nz; z++) {
		      		get(x, y, z).particles.remove_if(
		      			[this, &moved_particles, &x, &y, &z] (particle_t* p) {
			      			bool cell_changed = get(std::array<idx_type, 3>({x, y, z})) != get(p->position);
			      			if (cell_changed) {
			      				moved_particles.push_back(p);
			      			}
			      			return cell_changed;
		      			}
		      		);
		      		//std::cout << "x" << x << " y" << y << " z" << z << " " << get(x, y, z).particles.size() << std::endl;
		      	}
		    }
		}

		// iterate over all moved particles and add them to the appropiate cells
		for (particle_t*& particle_ptr : moved_particles) {
			get(particle_ptr->position).particles.push_back(particle_ptr);
		}
	}

	void push_back(particle_t& particle) {
	    get(particle.position).particles.push_back(&particle);
	}

	grid_cell_t& get(const coord_t& position) {
		std::array<idx_type, 3> cell_indices;
		cell_indices[0] = (idx_type) std::min<calc_t>(std::max<calc_t>(floor((position[0]+dimensions[0]/2.0)/smoothing_length), 0), Nx-1); 
	    cell_indices[1] = (idx_type) std::min<calc_t>(std::max<calc_t>(floor((position[1]+dimensions[1]/2.0)/smoothing_length), 0), Ny-1);
	    cell_indices[2] = (idx_type) std::min<calc_t>(std::max<calc_t>(floor((position[2]+dimensions[2]/2.0)/smoothing_length), 0), Nz-1);

	    return get(cell_indices);
	}

	grid_cell_t& get(idx_type x, idx_type y, idx_type z) {
		return cells[x + Nx*y + Nx*Ny*z];
	}

	grid_cell_t& get(const std::array<idx_type, 3>& idx) {
		return get(idx[0], idx[1], idx[2]);
	}
};

#endif
