#include <cstddef>
#include <tuple>
#include <Eigen/Dense>

typedef double calc_t;
typedef Eigen::Vector3d coord_t;

/*
 * 1. bool that checks whether collision occurs
 * 2. distance between particle and plane
 * 3. projected contact point of the particle and the plane
 */
typedef std::tuple<bool, calc_t, coord_t> collision_query_t;