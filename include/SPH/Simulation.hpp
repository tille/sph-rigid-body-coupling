#ifndef SPH_SIMULATION_HPP
#define SPH_SIMULATION_HPP

#include "types.hpp"
#include "SmoothingKernel.hpp"

#define SCALE 1

namespace SPH {

template <typename PHASE_SPACE_T>
class Simulation {
public:
  struct Parameter {
    calc_t smoothing_length = 0.0457/SCALE;
    calc_t dt = 0.01;
    calc_t particle_mass = 0.02/(SCALE*SCALE*SCALE);
    calc_t gas_stiffness = 5.0;
    calc_t rest_density = 998.29;
    calc_t rcut;
    calc_t viscosity = 3.5;
    calc_t surface_threshold = 7.065;
    calc_t surface_tension = 0.0728;
    calc_t radius = 0.01;
    coord_t gravity_acceleration;

    Parameter() : gravity_acceleration(0, -9.80665, 0), rcut(smoothing_length) {}
  };

  typedef PHASE_SPACE_T phase_space_t;

  const Parameter& params;

  // smoothing kernel functors
  const SmoothingKernel::WPoly6 Wpoly6;
  const SmoothingKernel::WSpiky WSpiky;
  const SmoothingKernel::WViscosity WViscosity;

  // world interaction
  std::function<void(Particle&, const SPH::Simulation<phase_space_t>&)> worldConstraint = nullptr;

  template <typename WORLD_CONSTRAIN_FUNCTOR_T = void*>
  Simulation(phase_space_t& phase_space,
             const Parameter& params_,
             const WORLD_CONSTRAIN_FUNCTOR_T& worldConstraint_ = nullptr) : 
    phase_space_(phase_space),
    params(params_),
    worldConstraint([&worldConstraint_] (Particle& particle, const SPH::Simulation<phase_space_t>& sim) {
        worldConstraint_(particle, sim);
    }),
    // init smoothing kernel functors
    Wpoly6(SmoothingKernel::WPoly6(params.smoothing_length)),
    WSpiky(SmoothingKernel::WSpiky(params.smoothing_length)),
    WViscosity(SmoothingKernel::WViscosity(params.smoothing_length)) {
    }

  Simulation(Simulation&) = delete; // copy ctor
  Simulation(Simulation&&) = default; // move ctor

  const std::size_t& iteration() const {
    return iteration_;
  }

  void step () {
    const calc_t h = params.smoothing_length;
    const calc_t dt = params.dt/2;

    /*
     * Update density & pressure
     */
    phase_space_.for_each(
      params.rcut,
      // pre
      [this] (Particle& particle) {
        particle.density = 0.0;
      },
      // run on all neighbours
      [this] (Particle& particle, Particle& neighbour) {
        particle.density += Wpoly6((particle.position - neighbour.position).squaredNorm());
      },
      // post
      [this] (Particle& particle) {
        particle.density += Wpoly6(0.0);

        particle.density *= params.particle_mass;
        particle.pressure = params.gas_stiffness * (particle.density - params.rest_density); // p = k(density - density_rest)
      }
    );

    /*
     * Compute forces
     */
    coord_t f_pressure,
              f_viscosity,
              f_surface,
              color_field_normal;

    calc_t color_field_laplacian;

    phase_space_.for_each(
      params.rcut,
      // pre
      [this, &f_pressure, &f_viscosity, &f_surface, &color_field_normal, &color_field_laplacian]
        (Particle& particle) {
        f_pressure.setConstant(0);
        f_viscosity.setConstant(0);
        f_surface.setConstant(0);
        color_field_normal.setConstant(0);
        color_field_laplacian=0;
      },
      // run on all neighbours
      [this, &f_pressure, &f_viscosity, &f_surface, &color_field_normal, &color_field_laplacian]
        (Particle& particle, Particle& neighbour) {
        const coord_t diff = particle.position - neighbour.position;
        calc_t r2 = diff.squaredNorm();
        
        if (particle != neighbour) {
          f_pressure += (particle.pressure/std::pow(particle.density,2)
                         + neighbour.pressure/std::pow(neighbour.density,2)
                        ) * WSpiky.gradient(diff, r2);
          f_viscosity += (neighbour.velocity - particle.velocity) * WViscosity.laplacian(r2) / neighbour.density;
        }
     
        color_field_normal += Wpoly6.gradient(diff, r2) / neighbour.density;
        color_field_laplacian += Wpoly6.laplacian(r2) / neighbour.density;
      },
      // post
      [this, &f_pressure, &f_viscosity, &f_surface, &color_field_normal, &color_field_laplacian]
        (Particle& particle) {
        color_field_normal += Wpoly6.gradient(coord_t(0, 0, 0), 0) / particle.density;
        color_field_laplacian += Wpoly6.laplacian(0) / particle.density;

        // collect
        f_pressure *= -params.particle_mass * particle.density;
        f_viscosity *= params.viscosity * params.particle_mass;
        color_field_normal *= params.particle_mass;
        particle.normal = -color_field_normal;
        
        color_field_laplacian *= params.particle_mass;
        
        // surface tension force
        calc_t color_field_normal_magnitude = color_field_normal.norm();
        
        if (color_field_normal_magnitude > params.surface_threshold) {
          particle.flag = true;
          f_surface = -params.surface_tension * color_field_normal / color_field_normal_magnitude * color_field_laplacian;
        }
        else {
          particle.flag = false;
        }
        
        // add sph forces
        particle.acceleration = (f_pressure + f_viscosity + f_surface) / particle.density + params.gravity_acceleration;
    
        assert(!isnan(particle.acceleration[0]) && !isnan(particle.acceleration[1]) && !isnan(particle.acceleration[2]));

        // apply external forces
        if (worldConstraint != nullptr)
          worldConstraint(particle, *this);
      }
    );
      
    /*
     * Update position and velocity (verlet)
     */
    coord_t new_position;
    coord_t new_velocity;
    phase_space_.for_each(
      params.rcut,
      // pre
      [this, &new_position, &new_velocity] (Particle& particle) {},
      // run on all neighbours
      [this] (Particle& particle, Particle& neighbour) {},
      // post
      [this, &new_position, &new_velocity, &dt] (Particle& particle) {
        new_position = particle.position + particle.velocity*dt + particle.acceleration*dt*dt;
        new_velocity = (new_position - particle.position) / dt;
        
        particle.position = new_position;
        particle.velocity = new_velocity;

        assert(!isnan(particle.position[0]) && !isnan(particle.position[1]) && !isnan(particle.position[2]));
      }
    );
      
    /*
    * Update the grid
    */
    phase_space_.update();
    
    // higher iteration count
    iteration_++;
  }
private:
  phase_space_t& phase_space_;
  std::size_t iteration_ = 0;
};

};

#endif