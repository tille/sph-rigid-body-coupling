#ifndef SHP_PARTICLE_HPP
#define SHP_PARTICLE_HPP

#include "types.hpp"

namespace SPH {

struct Particle {
    const std::size_t id;
    coord_t position;
    coord_t velocity;
    coord_t acceleration;
    coord_t normal;
    calc_t density;
    calc_t pressure;
    bool flag;

    static std::size_t count;

    Particle() : Particle(coord_t(0, 0, 0))  {}
    Particle(coord_t position_) : id(count++), position(position_), velocity(0, 0, 0), acceleration(0, 0, 0), normal(0, 0, 0), density(0), pressure(0) {}

    Particle(Particle&) = delete;
    Particle(Particle&&) = default;

    inline bool operator==(const Particle& particle) const { return id == particle.id; }
    inline bool operator!=(const Particle& particle) const { return !(*this==particle); }
};

}

#endif // SHP_PARTICLE_HPP
