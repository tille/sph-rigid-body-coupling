#ifndef SMOOTHING_KERNEL_HPP
#define SMOOTHING_KERNEL_HPP

#include "types.hpp"

namespace SPH {
	namespace SmoothingKernel {

// Wpoly6
struct WPoly6 {
	const calc_t h; // smoothing length

	WPoly6(calc_t h_) : h(h_) {}

	calc_t operator()(calc_t r2) const;

	coord_t gradient(coord_t diff, calc_t r2) const;

	calc_t laplacian(calc_t r2) const ;
};

// Wspiky
struct WSpiky {
	const calc_t h; // smoothing length

	WSpiky(calc_t h_) : h(h_) {}

	coord_t gradient (coord_t diff, calc_t r2) const;
};

// WViscosity
struct WViscosity {
	const calc_t h; // smoothing length

	WViscosity(calc_t h_) : h(h_) {}

	calc_t laplacian(calc_t r2) const;
};

	}
}
#endif