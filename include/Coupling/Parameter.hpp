#ifndef COUPLING_PARAMETER
#define COUPLING_PARAMETER

namespace Coupling {

struct Parameter {
	calc_t wall_k = 1000000.0;
	calc_t wall_damping = -2;
	calc_t collision_threshold;

	template <typename SPH_SIMULATION_PARAMS_T>
	Parameter(const SPH_SIMULATION_PARAMS_T& sph_sim_params) {
		collision_threshold = sph_sim_params.radius*3;
	} 
};

}
#endif