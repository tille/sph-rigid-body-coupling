#ifndef PHASE_SPACE_HPP
#define PHASE_SPACE_HPP
#include <cassert>
#include <vector>
#include <functional>   // std::bind
#include <cmath>
#include <boost/range/adaptor/filtered.hpp>

#include "SPH/Particle.hpp"

using namespace std::placeholders;

template <class T>
bool isNeighbour(calc_t rcut, const T& p1, const T & p2) {
	return p1 != p2 && (p1.position - p2.position).squaredNorm() < (rcut*rcut);
}

#include "Grid.hpp"

template <typename PARTICLE_T, typename CONTAINER_T>
struct PhaseSpaceVisitor {
	typedef PARTICLE_T particle_t;

	typedef CONTAINER_T container_t;

	typedef boost::range_detail::filtered_range<std::function<bool(const particle_t& p1)>, container_t> neighbour_container_t;

	template <typename PHASE_SPACE_T, typename F1, typename F2, typename F3>
	void for_each(PHASE_SPACE_T& phase_space, calc_t rcut, const F1& pre, const F2& interaction, const F3& post) {
		for (particle_t& particle : phase_space.particles()) {
			// apply pre func on current particle
			pre(particle);

			// get neighbours
			neighbour_container_t neighbours = boost::adaptors::filter(
				phase_space.particles(),
				static_cast<std::function<bool(const particle_t& p1)>>(std::bind(isNeighbour<particle_t>, rcut, _1, particle))
			);

			// interaction between particles and neighbours
			for (particle_t& neighbour : neighbours) {
				interaction(particle, neighbour);
			}

			// apply post func on current particle
			post(particle);
		}
	}

	void update() {}

	void push_back(particle_t& p) {}
};

template <typename PARTICLE_T, template <typename, typename> class VISITOR_T = PhaseSpaceVisitor>
class PhaseSpace {
public:
	typedef double calc_t;
	typedef PARTICLE_T particle_t;
	typedef std::vector<particle_t> container_t;
	typedef boost::range_detail::filtered_range<std::function<bool(const particle_t& p1)>, container_t> neighbour_container_t;

	const calc_t particle_mass; // mass of a single particle

	PhaseSpace(size_t n, calc_t particle_mass_, const coord_t dimensions_, const calc_t smoothing_length_)
		: particle_mass(particle_mass_),
		  visitor_(dimensions_, smoothing_length_)

	{
		particles_.reserve(n);
	};

	container_t& particles() {
		return particles_;
	}

	const container_t& particles() const {
		return particles_;
	}

	neighbour_container_t neighbours(const particle_t& particle, calc_t rcut) {
		return boost::adaptors::filter(
			particles_,
			static_cast<std::function<bool(const particle_t& p1)>>(std::bind(isNeighbour<particle_t>, rcut, _1, particle))
		);
	}

	template <typename F1, typename F2, typename F3>
	void for_each(calc_t rcut, const F1& pre, const F2& interaction, const F3& post) {
		visitor_.for_each(*this, rcut, pre, interaction, post);
	} 

	void push_back(particle_t&& p) {
		particles().push_back(std::move(p));
		visitor_.push_back(particles()[particles().size()-1]); // todo make sure particles() is not resized
	}

	std::size_t size() {
		return particles_.size();
	}

	void update() {
		visitor_.update();
	}

	coord_t center_of_mass() {
		coord_t cof(0, 0, 0);
		std::size_t nop = size();
		for (const particle_t& particle : particles()) {
			cof += particle.position;
			cof /= nop;
		}
		return cof;
		//cof /= params.particle_mass; //
	}

	calc_t max_norm() {
		calc_t n2=0;
		for (const particle_t& particle : particles()) {
			if (particle.position.squaredNorm() > n2) 
				n2 = particle.position.squaredNorm();
			
		}
		return std::sqrt(n2);
	}

	calc_t energy() {
		calc_t energy=0;
		for (const particle_t& particle : particles()) {
			energy += particle_mass/2*particle.velocity.squaredNorm()+9.81*particle_mass*particle.position[2];
		}
		return energy;
	}
private:
	container_t particles_;

	//PhaseSpaceVisitor<particle_t, container_t> visitor_;
	Grid<particle_t> visitor_;
};
#endif